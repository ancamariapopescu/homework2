import React, { Component } from 'react';
import './App.css';
import {ProductList} from './ProductList';
import {AddProduct} from './AddProduct';

class App extends Component {
  
 constructor(props){
    super(props);
    this.state={};
    this.state.productList=[];
  }
  
  async componentDidMount(){
    let productList = await this.getAllProducts();
    this.setState({
      productList: productList
    })
     console.log(productList);
  }
   getAllProducts = async () => {
    let response = await fetch('https://labs-ancamariapopescu.c9users.io:8090/get-all');
    let products = await response.json();
    return products;
  }
   updateProducts = (products) => {this.setState({ productList:products })}
   
  render() {
    return (
      <React.Fragment>
      <div>
      <h1>Products App</h1>
       <ProductList title="Product List" source={this.state.productList}/>
        <AddProduct source={this.state.productList} updateList={this.updateProducts}/>
  
      </div>
      </React.Fragment>
    );
  }
}

export default App;
