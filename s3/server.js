const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [{
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
});

app.post('/add', (req, res) => {
    if (req.body.productName && req.body.price) {
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    }
    else {
        res.status(500).send('Error!');
    }
});

app.put('/product/:id', (req, res) => {
    try {
        let found = false;
        products.forEach((product) => {
            if (product.id == req.params.id) {
                found = true;
                product.price = req.body.price;
                product.productName = req.body.productName;
                res.status(202).json(product);
            }
        });
        if(!found){
            res.status(404).json({message : 'not found'})
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'server error' })
    }
});

app.delete('/delete', (req, res) => {
    try {
        for (var i = 0; i < products.length; i++) {
            if (products[i].productName == req.body.productName) {
                products.splice(i, 1);
            }
        }
        res.status(200).json(products);
    }
    catch (e) {
        res.status(404).json({ message: 'not found' });
    }
});



app.listen(8080, () => {
    console.log('Server started on port 8080...');
});
